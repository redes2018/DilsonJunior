# Dilson Machado Fabião Junior
Contato: dilsonfabiaojunior@gmail.com

## Título do Projeto (TCC)
Monitoramento de infraestrutura de *datacenter* com dispositivo de baixo custo utilizando *Arduino*

### Orientador
Prof. Esp. Dartagnan Farias

### Tipos de Monitoramento:

* Temperatura (°C) e Umidade (%)
* Sensor de Gás (ppm)
* Sensor de Corrente (tensão de entrada e carga total)

### Links Importantes:
* [Wiki - Adamento](https://gitlab.com/redes2018/DilsonJunior/wikis/Andamento)
* [Wiki - Proposta](https://gitlab.com/redes2018/DilsonJunior/wikis/Proposta)
* [Wiki - Esquema/Layout](https://gitlab.com/redes2018/DilsonJunior/wikis/Esquema)
* [Artigo (Overleaf)](https://www.overleaf.com/read/ryynrpmrshbc)